Installation Instructions.
-------------------------

See below for installation on debian systems.

This package requires docbook-to-man, libbluetooth, dbus plus make and a C compiler.

To compile it use `make'

To install it use `make install'. 

The default installation directories are /usr/share, /usr/sbin,
/etc/bluemon. To install into /usr/local use `make PREFIX=/usr/local
install'. Individual files can be relocated by setting the appropriate make
variables:

   SBIN     - binaries ($PREFIX/sbin)
   BIN     - binaries ($PREFIX/bin)
   MAN     - man pages ($PREFIX/share/man)
   DOC     - documentation and examples ($PREFIX/share/doc/bluemon)
   CONFIG  - config file (/etc/bluemon)

It can be uninstalled with `make uninstall' with the same make variables set.

Starting Bluemon
----------------

The sample initscript should be copied to the system initscript directory and
used to start the program. It uses a config file to read the parameters to pass
to the program. The config file is searched for as /etc/default/bluemon,
/etc/bluemon.conf or /etc/bluemon/bluemon.conf. If not, use man or --help to
decide which parameters you wish to use. A good sample command line is:

bluemon -qai 100 -t 210 -b BLUETOOTHADDR

as root, and then as a user:

bluemon-client -u "UP COMMAND" -d "DOWN COMMAND" -b BLUETOOTHADDR

where UP COMMAND is executed when the signal passes the threshold upwards
and DOWN COMMAND when it passes the threshold downwards.

Debian
------

This packages build-depends on docbook-to-man,  libbluetooth1-dev, debhelper,
devscripts, dbus-1-dev, fakeroot and build-essential.

You will need to apply the debian diff first (should be distributed with the
sources), then you can build a Debian package with either `make deb' or any
of the normal methods of building a deb package (I suggest `debuild
-rfakeroot -us -uc')
